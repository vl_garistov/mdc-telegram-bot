/*
	MDC Telegram bot
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along
	with this program; if not, write to the Free Software Foundation, Inc.,
	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==================================================================================

	File: mdc-telegram-bot.ino
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Sends production status updates and alerts to a telegram chat
*/

#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include <WireSlave.h>
#include <stdlib.h>
#include "credentials.h"

#define SDA_PIN 21
#define SCL_PIN 22

#define BAUDRATE	115200
#define I2C_ADDRESS	0x33	// 0x3F is reserved for the I2C port expander used by the 16x2 LCD screen, don't use it.

// Sends a notification when REMAINING_WARNING units remain until a refil is needed
#define REMAINING_WARNING	10

typedef enum __attribute__((__packed__))
{
	NoEvent,
	EngravingBatchStarted,
	EngravingBatchFinished,
	EngravingBatchRemaining,
	EngravingBatchCancelled
}
mdc_prod_event_t;

// Add a union if logging for more subsystems is implemented
typedef struct __attribute__((__packed__))
{
	mdc_prod_event_t event;
	uint8_t remaining;
}
mdc_msg_t;

void I2C_receive_handler(int num_bytes);

const char msg_eng_batch_10_remaining[] = "10 engravings remain until rotary table reload.";
const char msg_eng_batch_started[] = "A new batch of engravings has been started.";
const char msg_eng_batch_finished[] = "Engraving batch finished. Please reload rotary table.";
const char msg_eng_batch_cancelled[] = "The current batch of engravings has been cancelled.";

WiFiClientSecure wifi_client;
UniversalTelegramBot mdc_prod_bot(bot_token, wifi_client);

volatile bool new_msg_flag = false;

void setup()
{
	Serial.begin(BAUDRATE);

	if (!WireSlave.begin(SDA_PIN, SCL_PIN, I2C_ADDRESS))
	{
		Serial.println("I2C slave initialisation failed.");
		while (1);
	}
	WireSlave.onReceive(I2C_receive_handler);

	Serial.print("Connecting to network: ");
	Serial.println(ssid);
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED)
	{
		Serial.print(".");
		delay(500);
	}
	Serial.print("\nWiFi connected\nIP address: ");
	Serial.println(WiFi.localIP());

	wifi_client.setCACert(TELEGRAM_CERTIFICATE_ROOT);

	mdc_prod_bot.sendMessage(chat_id_status_updates, "Bot started up.", "");
}

void loop()
{
	mdc_msg_t new_msg;
	static size_t received_bytes = 0;
	char rem_buf[25];

	WireSlave.update();
	
	if (new_msg_flag)
	{
		new_msg_flag = false;
		while (WireSlave.available())
		{
			((uint8_t *) &new_msg)[received_bytes] = WireSlave.read();
			received_bytes++;
			if (received_bytes == sizeof(mdc_msg_t))
			{
				received_bytes = 0;
				switch (new_msg.event)
				{
					case EngravingBatchStarted:
						mdc_prod_bot.sendMessage(chat_id_status_updates, msg_eng_batch_started, "");
						break;
					case EngravingBatchFinished:
						mdc_prod_bot.sendMessage(chat_id_status_updates, msg_eng_batch_finished, "");
						break;
					case EngravingBatchRemaining:
						if (new_msg.remaining == 10)
						{
							mdc_prod_bot.sendMessage(chat_id_status_updates, msg_eng_batch_10_remaining, "");
						}
						sprintf(rem_buf, "%d engravings remain", new_msg.remaining);
						mdc_prod_bot.sendMessage(chat_id_rt_updates, rem_buf, "");
						break;
					case EngravingBatchCancelled:
						mdc_prod_bot.sendMessage(chat_id_status_updates, msg_eng_batch_cancelled, "");
						break;
					default:;
				}
			}
		}
	}
}

void I2C_receive_handler(int num_bytes)
{
	new_msg_flag = true;
}