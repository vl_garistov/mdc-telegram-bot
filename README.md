# MDC - Telegram bot

Event log bot for internal use by MDC. Provides important notifications about the production activities at the company.

This code runs on an ESP32 connected over I2C to the Arduino Nano in [mdc-laser-stepper](https://gitlab.com/vl_garistov/mdc-laser_stepper). It receives messages from it and sends corresponding messages to two Telegram group chats depending on the type of message. Messages about the number of remaining engravings are sent to "MDC - real time updates". If precisely 10 engravings remain, an additional message is also sent to "MDC - status updates". Messages about starting, finishing and cancelling a batch of engravings are sent to "MDC - status updates". 

The file credentials.h contains the constants that are used in this program but are seemingly undefined. They contain authentication data and must not be published.